��          �       <      <     =     K     O     V  
   [  
   f     q     z     �  	   �     �     �     �  
   �  �   �  D   P     �  �  �     >     L     Q     W     \     h  
   t           �     �     �     �     �     �  �   �  U   m     �   ACF Filtering All Blocks Bulk By default Categories Category Comments Field Group Category : Filtering Medias Menus Options Pages Post Types The ACF Filtering plugin cannot be activated because the following required plugins are not active: %s. Please activate these plugins The ideal plugin to find your way easily among your ACF groupfields. Users Project-Id-Version: ACF Filtering
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-05-12 13:00+0000
PO-Revision-Date: 2021-05-12 13:05+0000
Last-Translator: 
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.2; wp-5.7.1
X-Domain: krr-acffg ACF Filtering Tous Blocs Vrac Par défaut Catégories Catégorie Commentaires Catégorie de groupe de champs : Filtrage Médias Menus Pages d'options Types de contenu Le plugin ACF Filtering n'a pas pu être activé car les plugins requis suivants ne sont pas actifs : %s. Merci d'activer ces plugins.  Le plugin idéal pour s'y retrouver facilement parmi tous les groupes de champs ACF ! Utilisateurs 