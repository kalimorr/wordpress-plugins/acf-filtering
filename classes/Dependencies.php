<?php

namespace KrrAcfFiltering;

/**
 * Class Dependencies
 * @package KrrAcfFiltering
 */
class Dependencies
{
	/**
	 * @var Dependencies|null  Instance de la class Dependencies
	 */
	public static $instance = null;

	private $dependencies = [
		[
			'advanced-custom-fields-pro/acf.php' => 'Advanced Custom Fields PRO',
			'advanced-custom-fields/acf.php'     => 'Advanced Custom Fields'
		]
	];

	public $missingDependencies = [];

	/**
	 * Get the instance of the current class
	 *
	 * @return Dependencies()|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Dependencies();
		}

		return self::$instance;
	}

	public function check ()
	{
		/* Needed to the function "is_plugin_active" works */
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		/* Check each dependency */
		foreach ($this->dependencies as $dependencyPath => $dependencyName) {

			/* Case when there is a choice for multiple plugins */
			if (is_array($dependencyName)) {
				$missing = null;
				/* If at least one of the plugins in the list is activated, it is good */
				foreach ($dependencyName as $subdependencyPath => $subdependencyName) {
					if (is_plugin_active($subdependencyPath)) {
						$missing = false;
						break;
					} else {
						$missing = $subdependencyName;
					}
				}
				if ($missing) {
					array_push($this->missingDependencies, $missing);
				}
			} else {
				if (!is_plugin_active($dependencyPath)) {
					array_push($this->missingDependencies, $dependencyName);
				}
			}
		}

		/* If some plugins are missing, show a notice et stop the activation */
		if ($this->missingDependencies) {
			add_action('all_admin_notices', [$this, 'dependencyNotice'], 5);
			add_action('admin_init', [$this, 'stopActivation'], 5);
		}
	}

	/**
	 * Notice if there are some missing dependencies
	 */
	public function dependencyNotice ()
	{
		$message = __('The ACF Filtering plugin cannot be activated because the following required plugins are not active: %s. Please activate these plugins', 'krr-acffg');

		$tpl = '<div class="error notice"><p><strong>';
		$tpl .= sprintf($message, implode(', ', $this->missingDependencies));
		$tpl .= '</strong></p></div>';

		echo $tpl;
	}

	/**
	 * Stop the plugin activation
	 */
	public function stopActivation ()
	{
		deactivate_plugins( plugin_basename( KRR_ACFFG_FILE ) );

		/* Disable the notice saying "Plugin activated" */
		if (isset($_GET['activate'])) {
			unset($_GET['activate']);
		}
	}
}