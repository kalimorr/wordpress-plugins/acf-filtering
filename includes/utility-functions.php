<?php

namespace KrrAcfFiltering;

/**
 * Returns the plugin path to a specified file.
 *
 * @param string $filename
 *
 * @return string
 */
function krr_get_path( $filename = '' ) {
	return plugin_dir_path(KRR_ACFFG_FILE) . ltrim($filename, '/');
}

/**
 * Includes a file within a Kofinorr plugin.
 *
 * @param string $filename
 */
function krr_include($filename = '') {
	$file_path = krr_get_path($filename);
	if(file_exists($file_path)) {
		include_once($file_path);
	}
}

/**
 * Requires a file within a Kofinorr plugin.
 *
 * @param string $filename
 */
function krr_require($filename = '') {
	$file_path = krr_get_path($filename);
	if(file_exists($file_path)) {
		require_once($file_path);
	}
}