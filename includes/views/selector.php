<?php namespace KrrAcfFiltering;

$screen = get_current_screen();

/* If we are editing a group fields, we get his category */
if ( $screen->action !== 'add' ) {
	$filtersTaxonomy = get_the_terms(get_the_ID(), Categories::getInstance()->getTaxonomyName());
	if (is_iterable($filtersTaxonomy) && !empty($filtersTaxonomy)) {
		$firstKey = (function_exists('array_key_first')) ? array_key_first($filtersTaxonomy) : 0;
		$postFilter = isset($filtersTaxonomy[$firstKey]) ? $filtersTaxonomy[$firstKey] : null;
	}
} else {
	$postFilter = null;
}
?>

<div class="krrAcfFiltering-selector">
	<label><?= __('Field Group Category :', 'krr-acffg') ?></label>

	<select name="<?= Plugin::getInstance()->selectorFieldName ?>" required>
		<?php foreach (Categories::getInstance()->getReducedList() as $slug => $label) {
			$selected = (is_object($postFilter) && $postFilter->slug === $slug) ? 'selected' : ''; ?>
			<option value="<?= $slug ?>" <?= $selected ?>><?= $label ?></option>
		<?php } ?>
	</select>
</div>
