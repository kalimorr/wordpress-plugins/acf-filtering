<?php

namespace KrrAcfFiltering;

/**
 * Class Plugin
 *
 * @package KrrAcfFiltering
 */
class Plugin
{
	/**
	 * @var string Name of the selector field in edit page
	 */
	public $selectorFieldName = 'krr-acf-filtering';

	/**
	 * @var Plugin|null  Instance de la class Plugin
	 */
	public static $instance = null;

	/**
	 * Plugin constructor.
	 */
	public function __construct()
	{
		$dependencies = Dependencies::getInstance();
		$dependencies->check();

		/* Continue only if no dependencies are missing */
		if (count($dependencies->missingDependencies) === 0) {
			add_action('plugins_loaded', [$this, 'load_i18n'], 1);
			add_action('plugins_loaded', [$this, 'loadFilters'], 2);
			add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 11);
			add_action('manage_posts_extra_tablenav', [$this, 'filterList']);
			add_action('edit_form_after_title', [$this, 'selectorList']);
			add_action('admin_menu', [$this, 'addMenuPage'], 15);

			new TaxonomyList();
			new Update();
		}
	}

	/**
	 * Load plugin textdomain
	 */
	public function load_i18n()
	{
		load_plugin_textdomain(
			'krr-acffg',
			false,
			dirname(plugin_basename(KRR_ACFFG_FILE)) . '/languages'
		);
	}

	/**
	 * Load filters
	 */
	public function loadFilters()
	{
		$filters = Categories::getInstance();
		$filters->init();
	}

	/**
	 * Get the instance of the current class
	 *
	 * @return Plugin|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Plugin();
		}

		return self::$instance;
	}

	/**
	 *  Styles and scripts
	 */
	public function enqueueScripts()
	{
		wp_enqueue_style('krr-acffg', plugin_dir_url(KRR_ACFFG_FILE) . 'assets/css/style.css', [], false);
		wp_enqueue_script('krr-acffg', plugin_dir_url(KRR_ACFFG_FILE) . 'assets/script.js', [], false, true);
	}

	/**
	 * Add the category menu entry
	 */
	public function addMenuPage()
	{
		add_submenu_page(
			'edit.php?post_type=acf-field-group',
			__('Categories', 'krr-acffg'),
			__('Categories', 'krr-acffg'),
			'manage_options',
			'edit-tags.php?taxonomy=' . Categories::getInstance()->getTaxonomyName(),
			'',
			5
		);
	}

	/**
	 * Switcher on edit page of an ACF field group
	 */
	public function selectorList()
	{
		$screen = get_current_screen();
		if ('acf-field-group' !== $screen->id) {
			return;
		}

		krr_require('includes/views/selector.php');
	}

	/**
	 * Switcher on list page of ACF
	 */
	public function filterList($which)
	{
		if (!$this->isListPage() || $which === 'top') {
			return;
		}

		krr_require('includes/views/filter.php');
	}

	/**
	 * Check if we are in the list page of ACF field groups
	 *
	 * @return bool
	 */
	private function isListPage()
	{
		$screen = get_current_screen();
		return 'edit-acf-field-group' === $screen->id && !isset($_GET['s']);
	}
}