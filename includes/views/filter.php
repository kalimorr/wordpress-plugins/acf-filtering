<?php namespace KrrAcfFiltering;

$currentFilter = $_GET[Categories::getInstance()->getTaxonomyName()] ?? null;
$baseUrl = admin_url('edit.php') . '?post_type=acf-field-group';
?>

<ul class="krrAcfFiltering-filters">
	<li class="label"><span><?= __('Filtering', 'krr-acffg') ?></span></li>

	<?php /* Show all group fields */ ?>
	<li data-prefix="all" class="<?= (($currentFilter === null) ? 'active' : '') ?>">
		<a href="<?= $baseUrl ?>"><?= __('All', 'krr-acffg') ?></a>
	</li>

	<?php foreach (Categories::getInstance()->getList(true) as $slug => $label) {
		/* Add the filter parameter on the url */
		$url = $baseUrl . '&' . Categories::getInstance()->getTaxonomyName() . '=' . $slug; ?>

		<li data-prefix="<?= $slug ?>" class="<?= ($slug === $currentFilter) ? 'active' : '' ?>">
			<a href="<?= $url ?>"><?= $label ?></a>
		</li>
	<?php } ?>

</ul>