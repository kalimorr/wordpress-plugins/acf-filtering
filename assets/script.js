(function ($, doc, win) {
	'use strict'

	/* Plugin Handler */
	win.KrrAcfFiltering = {

		/**
		 * Initialization method
		 */
		menuInit: function () {
			var $link = $('a.current');

			if ($link.length === 0) {
				return;
			}

			/* Make the right menu item active */
			if ($link.attr('href').endsWith('krr-acf-filtering')) {

				var $actualCurrent = $('li.menu-top.wp-has-current-submenu');
				$actualCurrent.removeClass('wp-has-current-submenu wp-menu-open');
				$actualCurrent.find('a.menu-top').removeClass('wp-has-current-submenu wp-menu-open');

				$link.parents('.menu-top')
				     .removeClass('wp-not-current-submenu')
				     .addClass('wp-has-current-submenu wp-menu-open');

				$link.parents('.menu-top').find('a.menu-top')
				     .removeClass('wp-not-current-submenu')
				     .addClass('wp-has-current-submenu wp-menu-open');
			}
		}
	};

	/* Admin page ready */
	$(doc).ready(function () {
		KrrAcfFiltering.menuInit();
	});
})(jQuery, document, window);